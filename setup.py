# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='cytostat',
    version='1.0',
    package_dir={'': 'cytostat'},
    packages=find_packages(where='cytostat'),
    package_data={'' : ['checkpoints/*']},
    author='Alexandre Bouyssoux',
    license='LICENSE.txt',
    description='Package to compute statistics on cytology digital slides',
    url='https://gitlab.com/vitadx/articles/automated_staining_analysis',
    classifiers=[],
)

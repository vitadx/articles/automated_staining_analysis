# automated_staining_analysis

Code for the project:

Alexandre Bouyssoux, Kathleen Jarnouen, Laetitia Lallement, Riadh Fezzani, Jean-Christophe Olivo-Marin, *Automated Staining Analysis in Digital Cytopathology and Applications*.


- Source code is located under the cytostat folder.
- A jupyter notebook is provided to demonstrate the main functionalities of the software.
- Data for the notebook demonstration can be downloaded [here](https://extranet.vdx-sys.ovh/s/x6cbGH7X37kc8Ws) and placed under `./data/slides/` for direct use.

# -*- coding: utf-8 -*-

import numpy as np
from skimage import img_as_float, img_as_ubyte, morphology, feature, color
from scipy import ndimage


################################################################################
# General
################################################################################


def compute_normed_value(value1, value2):
    """compute standard deviation of data's region defined by
    mask_area normalized by the standard deviation of data's region defined by
    mask_bg

    Parameters:
    -----------
    mask_area : ndarray
        array with the same shape as data, defining the region over which is
        computed the standard deviation
    mask_bg : ndarray
        array with the same shape as data, defining the background region used
        for normalization.
    """
    if value1 != value1 or value2 != value2: # check if nan
        normed_value = np.nan
    elif value1 == 0 or value2 == 0:
        normed_value = np.nan
    else:
        normed_value = value1 / value2
    return normed_value


def region_median(data, mask):
    """compute median value over data's region defined by mask

    Parameters:
    -----------
    data : 2darray
        data of interest : should correspond to luminance
    mask : 2darray
        array with the same shape as data, defining the region over which is
        computed the median intensity
    """
    data = img_as_float(data)
    mask = mask.astype(bool)
    mask = morphology.binary_erosion(mask, selem=np.ones((5,5)))
    if data.ndim != 2:
        raise ValueError('data should have 2 dims, not', data.ndim)
    if data.shape != mask.shape:
        msg = f'mask shape {mask.shape} != from data shape {data.shape}'
        raise ValueError(msg)
    if np.sum(mask) == 0:
        i = np.nan
    else:
        i = np.median(data[mask])
    return i


def compute_std(data, mask):
    """compute standard deviation over data's region defined by mask

    Parameters:
    -----------
    data : ndarray
        data of interest
    mask : ndarray
        array with the same shape as data, defining the region over which is
        computed the standard deviation
    """
    if data.shape != mask.shape:
        msg = f'mask shape {mask.shape} != from data shape {data.shape}'
        raise ValueError(msg)
    data = img_as_float(data)
    mask = mask.astype(bool)
    mask = morphology.binary_erosion(mask, selem=np.ones((5,5)))
    if np.sum(mask) == 0:
        std = np.nan
    else:
        std = np.std(data[mask])
    return std


################################################################################
# Luminance Metrics
################################################################################


def region_optical_density(area_transmission):
    """compute tenegrad metric over data's region defined by mask

    Parameters:
    -----------
    area_transmission : float
        luminance transmission as computed by region_transmission.
    """
    return - np.log10(area_transmission)


################################################################################
# Texture metrics
################################################################################


def compute_tenengrad(data, mask):
    """compute Tenengrad metric over data's region defined by mask

    Parameters:
    -----------
    data : 2darray
        data of interest
    mask : 2darray
        array with the same shape as data, defining the region over which is
        computed the tenegrad metric
    """
    if data.ndim != 2:
        raise ValueError('data should have 2 dims, not', data.ndim)
    if data.shape != mask.shape:
        msg = f'mask shape {mask.shape} != from data shape {data.shape}'
        raise ValueError(msg)
    data = img_as_float(data)
    mask = mask.astype(bool)
    mask = morphology.binary_erosion(mask, selem=np.ones((7,7)))
    if np.sum(mask) == 0:
        out = np.nan
    else:
        grad_h = ndimage.sobel(data, axis=0)
        grad_v = ndimage.sobel(data, axis=1)
        tenegrad = ndimage.uniform_filter(grad_h**2 + grad_v**2, size=3)
        out = np.sum(tenegrad[mask]) / np.sum(mask)
    return out


def compute_minmax(data, mask):
    """compute median intensity over data's region defined by mask

    Parameters:
    -----------
    data : 2darray
        data of interest : should correspond to luminance
    mask : 2darray
        array with the same shape as data, defining the region over which is
        computed the median intensity
    """
    data = img_as_float(data)
    mask = mask.astype(bool)
    mask = morphology.binary_erosion(mask, selem=np.ones((5,5)))
    if data.ndim != 2:
        raise ValueError('data should have 2 dims, not', data.ndim)
    if data.shape != mask.shape:
        msg = f'mask shape {mask.shape} != from data shape {data.shape}'
        raise ValueError(msg)
    if np.sum(mask) == 0:
        i = np.nan
    else:
        i = np.amax(data[mask]) - np.amin(data[mask])
    return i


def compute_glcm(data, mask):
    """compute Grey Level Co-occurence matrix on masked region of data

    Parameters:
    -----------
    data : 2darray
        data of interest : should correspond to luminance
    mask : 2darray
        array with the same shape as data, defining the region over which is
        computed the GLCM
    """
    if data.ndim != 2:
        raise ValueError('data should have 2 dims, not', data.ndim)
    if data.shape != mask.shape:
        msg = f'mask shape {mask.shape} != from data shape {data.shape}'
        raise ValueError(msg)
    if data.dtype != np.uint8:
        data = img_as_ubyte(data)
    masked_data = data.astype(np.int16)
    mask = mask.astype(bool)
    mask = morphology.binary_erosion(mask, selem=np.ones((5,5)))
    masked_data[~mask] = -1
    masked_data += 1
    glcm = feature.greycomatrix(masked_data, distances=[5],
                                angles=[0, np.pi/4, np.pi/2, 3*np.pi/4],
                                levels=257,
                                symmetric=True,
                                normed=False)
    # remove first row and col corresponding to background:
    glcm = glcm[1:, 1:, ...].astype(np.float64)
    glcm_norm = np.sum(glcm, axis=(0,1))

    if glcm_norm.min() == 0 or np.isnan(np.sum(glcm_norm)):
        # nan if one of the computed GLCM contains only zeros
        # can happen when mask is empty or too small
        glcm = np.empty((256, 256, 1, 4))
        glcm.fill(np.nan)
    else:
        glcm /= np.sum(glcm, axis=(0,1))

    return glcm


def glcm_energy(P):
    """compute energy from a GLCM

    Parameters:
    -----------
    P : ndarray
        Grey level co-occurence matrix
    """
    energy = np.mean(feature.greycoprops(P, prop='energy'))
    return energy


def glcm_entropy(P):
    """compute entropy from a GLCM

    Parameters:
    -----------
    P : ndarray
        Grey level co-occurence matrix
    """
    assert P.ndim == 4, print('P should have 4 dim, but has', P.ndim)
    if np.sum(P) != np.sum(P):
        entropy = np.nan
    else:
        # compute entropy matrix:
        h = np.empty((P.shape[-2], P.shape[-1]))
        for angle in range(P.shape[-1]):
            for d in range(P.shape[-2]):
                M = P[:, :, d, angle] # glcm for one distance and one angle
                h[d, angle] = np.sum(-M * np.log(M + 1e-12))
        # compute mean entropy:
        entropy = np.mean(h)
    return entropy


def glcm_homogeneity(P):
    """compute homogeneity from a GLCM

    Parameters:
    -----------
    P : ndarray
        Grey level co-occurence matrix
    """
    homogeneity = np.mean(feature.greycoprops(P, prop='homogeneity'))
    return homogeneity


def compute_lbp_hist(data, mask):
    """compute local binary pattern histogram

    Parameters:
    -----------
    data : ndarray
        data of interest
    mask : ndarray
        array with the same shape as data, defining the region over which is
        computed the local binary patterns
    """
    assert data.ndim == 2, 'data input expected to be of dim 2 (grayscale)'
    mask = mask.astype(bool)
    # P=8 for 256 bins histogram. R=5 to match GLCM radius. rotation invariant.
    lbp = feature.local_binary_pattern(data, P=8, R=5, method='ror')
    lbp = lbp[mask] # filter texture on region defined by mask
    lbp = np.histogram(lbp.ravel(), bins=np.arange(257), density=True)[0]
    return lbp


if __name__ == '__main__':
    pass

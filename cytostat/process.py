# -*- coding: utf-8 -*-

import os
import numpy as np
import pandas as pd
import imageio
import skimage
from sklearn import cluster
from tqdm import tqdm

from . import metrics, utils
from .constants import BOUND_L


def slide_stats(img_folder, bgd_mask_folder, cyt_mask_folder, ncl_mask_folder,
    lvlmap_folder=None, intensity=True, color=True, glcm_texture=True,
    lbp_texture=True, find_blue_cells=True):
    """compute stats for one slide

    Parameters:
    -----------
    img_folder : str
        path of the folder containing the images to be analyzed.
    bgd_mask_folder : str
        path of the folder containing the background region masks.
    cyt_mask_folder : str
        path of the folder containing the cytoplasm region masks.
    ncl_mask_folder : str
        path of the folder containing the nucleus region masks.
    lvlmap_folder : str, optional
        path of the folder containing the edf level maps, needed to compute
        texture features based on the level map min-max. If None, the levelmap
        texture features are not computed.
    intensity : boolean, optional
        if True, add intensity statistics.
    color : boolean, optional
        if True, add color statistics.
    glcm_texture : boolean, optional
        if True, compute GLCM image texture statistics.
    lbp_texture : boolean, optional
        if True, compute Local binary patterns image texture statistics.
    find_blue_cells : boolean, optional
        if True, apply 2-class unsupervised clustering to identify blue cells
        from red cells.

    Returns:
    --------
    df : pandas.DataFrame
        dataframe containing the color statistics. Each row is a cell.
    """
    assert os.path.exists(img_folder), f'{img_folder} not found !'
    assert os.path.exists(bgd_mask_folder), f'{bgd_mask_folder} not found !'
    assert os.path.exists(cyt_mask_folder), f'{cyt_mask_folder} not found !'
    assert os.path.exists(ncl_mask_folder), f'{ncl_mask_folder} not found !'
    if lvlmap_folder is None:
        lvl_texture = False
    else:
        assert os.path.exists(lvlmap_folder), f'{lvlmap_folder} not found !'

    index = [] # init dataframe index
    mdict = {} # init dataframe data
    if intensity:
        mdict['nucleus_intensity'] = []
        mdict['cytoplasm_intensity'] = []
        mdict['background_intensity'] = []
        mdict['nucleus_transmission'] = []
        mdict['cytoplasm_transmission'] = []
        mdict['nucleus_OD'] = []
        mdict['cytoplasm_OD'] = []
        mdict['contrast'] = []
    if color:
        mdict['nucleus_a'] = []
        mdict['nucleus_b'] = []
        mdict['cytoplasm_a'] = []
        mdict['cytoplasm_b'] = []
        mdict['background_a'] = []
        mdict['background_b'] = []
    if glcm_texture:
        # GLCM tenengrad
        mdict['nucleus_tenengrad'] = []
        mdict['cytoplasm_tenengrad'] = []
        mdict['background_tenengrad'] = []
        mdict['nucleus_normed_tenengrad'] = []
        mdict['cytoplasm_normed_tenengrad'] = []
        # GLCM energy
        mdict['nucleus_energy'] = []
        mdict['cytoplasm_energy'] = []
        mdict['background_energy'] = []
        mdict['nucleus_normed_energy'] = []
        mdict['cytoplasm_normed_energy'] = []
        # GLCM entropy
        mdict['nucleus_entropy'] = []
        mdict['cytoplasm_entropy'] = []
        mdict['background_entropy'] = []
        mdict['nucleus_normed_entropy'] = []
        mdict['cytoplasm_normed_entropy'] = []
        # GLCM homogeneity
        mdict['nucleus_homogeneity'] = []
        mdict['cytoplasm_homogeneity'] = []
        mdict['background_homogeneity'] = []
        mdict['nucleus_normed_homogeneity'] = []
        mdict['cytoplasm_normed_homogeneity'] = []
    if lbp_texture:
        mdict['nucleus_lbp'] = []
        mdict['cytoplasm_lbp'] = []
    if lvl_texture:
        mdict['nucleus_lvl_minmax'] = []
        mdict['cytoplasm_lvl_minmax'] = []
        mdict['background_lvl_minmax'] = []
        mdict['nucleus_lvl_normed_minmax'] = []
        mdict['cytoplasm_lvl_normed_minmax'] = []
        mdict['ratio_lvl_minmax'] = []

    for file in tqdm(os.scandir(img_folder), total=len(os.listdir(img_folder))):
        index.append(os.path.splitext(file.name)[0])

        img = skimage.color.rgb2lab(imageio.imread(file.path))
        mask_bgd = imageio.imread(os.path.join(bgd_mask_folder, file.name))
        mask_cyt = imageio.imread(os.path.join(cyt_mask_folder, file.name))
        mask_ncl = imageio.imread(os.path.join(ncl_mask_folder, file.name))
        if img.shape[0:2] != mask_bgd.shape: # in case predicted mask is cropped
            img = utils.ImageDataset.crop_nearest_multiple(img, multiple=8,
                                                           axis=(0,1))
        L, A, B = img[..., 0], img[..., 1], img[..., 2]

        if intensity:

            # compute region intensity:
            ncl_int = metrics.region_median(L, mask_ncl)
            cyt_int = metrics.region_median(L, mask_cyt)
            bgd_int = metrics.region_median(L, mask_bgd)
            mdict['nucleus_intensity'].append(ncl_int)
            mdict['cytoplasm_intensity'].append(cyt_int)
            mdict['background_intensity'].append(bgd_int)

            # compute region transmission:
            ncl_trans = metrics.compute_normed_value(ncl_int, bgd_int)
            cyt_trans = metrics.compute_normed_value(cyt_int, bgd_int)
            mdict['nucleus_transmission'].append(ncl_trans)
            mdict['cytoplasm_transmission'].append(cyt_trans)

            # compute region optical density:
            ncl_od = metrics.region_optical_density(ncl_trans)
            cyt_od = metrics.region_optical_density(cyt_trans)
            mdict['nucleus_OD'].append(ncl_od)
            mdict['cytoplasm_OD'].append(cyt_od)

            # compute nucleus cytoplasm contrast
            contrast = 1.0 - metrics.compute_normed_value(ncl_int, cyt_int)
            mdict['contrast'].append(contrast)

        if color:

            # compute region normed a (works with using region intensity function):
            bgd_a = metrics.region_median(A, mask_bgd)
            ncl_a = metrics.region_median(A, mask_ncl) - bgd_a
            cyt_a = metrics.region_median(A, mask_cyt) - bgd_a
            mdict['nucleus_a'].append(ncl_a)
            mdict['cytoplasm_a'].append(cyt_a)
            mdict['background_a'].append(bgd_a)

            # compute region normed b (works with using region intensity function):
            bgd_b = metrics.region_median(B, mask_bgd)
            ncl_b = metrics.region_median(B, mask_ncl) - bgd_b
            cyt_b = metrics.region_median(B, mask_cyt) - bgd_b
            mdict['nucleus_b'].append(ncl_b)
            mdict['cytoplasm_b'].append(cyt_b)
            mdict['background_b'].append(bgd_b)

        if glcm_texture:
            data = L / BOUND_L[1] # normalized L (Luminance) channel

            # compute region intensity tenegrad metric
            ncl_ten = metrics.compute_tenengrad(data, mask_ncl)
            cyt_ten = metrics.compute_tenengrad(data, mask_cyt)
            bgd_ten = metrics.compute_tenengrad(data, mask_bgd)
            mdict['nucleus_tenengrad'].append(ncl_ten)
            mdict['cytoplasm_tenengrad'].append(cyt_ten)
            mdict['background_tenengrad'].append(bgd_ten)

            # compute region normed intensity tenegrad metric
            norm_ncl_t = metrics.compute_normed_value(ncl_ten, bgd_ten)
            norm_cyt_t = metrics.compute_normed_value(cyt_ten, bgd_ten)
            mdict['nucleus_normed_tenengrad'].append(norm_ncl_t)
            mdict['cytoplasm_normed_tenengrad'].append(norm_cyt_t)

            # compute region intensity GLCM metrics

            bgd_glcm = metrics.compute_glcm(data, mask_bgd)
            bgd_glcm_energy = metrics.glcm_energy(bgd_glcm)
            bgd_glcm_entropy = metrics.glcm_entropy(bgd_glcm)
            bgd_glcm_homogen = metrics.glcm_homogeneity(bgd_glcm)
            mdict['background_energy'].append(bgd_glcm_energy)
            mdict['background_entropy'].append(bgd_glcm_entropy)
            mdict['background_homogeneity'].append(bgd_glcm_homogen)

            ncl_glcm = metrics.compute_glcm(data, mask_ncl)
            ncl_glcm_energy = metrics.glcm_energy(ncl_glcm)
            ncl_glcm_entropy = metrics.glcm_entropy(ncl_glcm)
            ncl_glcm_homogen = metrics.glcm_homogeneity(ncl_glcm)
            mdict['nucleus_energy'].append(ncl_glcm_energy)
            mdict['nucleus_entropy'].append(ncl_glcm_entropy)
            mdict['nucleus_homogeneity'].append(ncl_glcm_homogen)

            norm_ncl_glcm_energy = ncl_glcm_energy / bgd_glcm_energy
            norm_ncl_glcm_entropy = ncl_glcm_entropy / bgd_glcm_entropy
            norm_ncl_glcm_homogen = ncl_glcm_homogen / bgd_glcm_homogen
            mdict['nucleus_normed_energy'].append(norm_ncl_glcm_energy)
            mdict['nucleus_normed_entropy'].append(norm_ncl_glcm_entropy)
            mdict['nucleus_normed_homogeneity'].append(norm_ncl_glcm_homogen)

            cyt_glcm = metrics.compute_glcm(data, mask_cyt)
            cyt_glcm_energy = metrics.glcm_energy(cyt_glcm)
            cyt_glcm_entropy = metrics.glcm_entropy(cyt_glcm)
            cyt_glcm_homogen = metrics.glcm_homogeneity(cyt_glcm)
            mdict['cytoplasm_energy'].append(cyt_glcm_energy)
            mdict['cytoplasm_entropy'].append(cyt_glcm_entropy)
            mdict['cytoplasm_homogeneity'].append(cyt_glcm_homogen)

            norm_cyt_glcm_energy = cyt_glcm_energy / bgd_glcm_energy
            norm_cyt_glcm_entropy = cyt_glcm_entropy / bgd_glcm_entropy
            norm_cyt_glcm_homogen = cyt_glcm_homogen / bgd_glcm_homogen
            mdict['cytoplasm_normed_energy'].append(norm_cyt_glcm_energy)
            mdict['cytoplasm_normed_entropy'].append(norm_cyt_glcm_entropy)
            mdict['cytoplasm_normed_homogeneity'].append(norm_cyt_glcm_homogen)

        if lbp_texture:
            data = L / BOUND_L[1] # normalized L (Luminance) channel

            # compute region intensity LBP metrics
            ncl_lbp = metrics.compute_lbp_hist(data, mask_ncl) # nucleus
            cyt_lbp = metrics.compute_lbp_hist(data, mask_cyt) # cytoplasm
            mdict['nucleus_lbp'].append(ncl_lbp)
            mdict['cytoplasm_lbp'].append(cyt_lbp)

        if lvl_texture:
            data = imageio.imread(os.path.join(lvl_folder, file.name))
            if data.shape[0:2] != mask_bgd.shape:
                data = utils.ImageDataset.crop_nearest_multiple(
                    data, multiple=8, axis=(0,1))

            # compute region intensity standard deviation
            ncl_minmax = metrics.compute_minmax(data, mask_ncl)
            cyt_minmax = metrics.compute_minmax(data, mask_cyt)
            bgd_minmax = metrics.compute_minmax(data, mask_bgd)
            mdict['nucleus_lvl_minmax'].append(ncl_minmax)
            mdict['cytoplasm_lvl_minmax'].append(cyt_minmax)
            mdict['background_lvl_minmax'].append(bgd_minmax)

            # compute region normed levelmap values minmax
            norm_ncl_minmax = metrics.compute_normed_value(ncl_minmax,
                                                           bgd_minmax)
            norm_cyt_minmax = metrics.compute_normed_value(cyt_minmax,
                                                           bgd_minmax)
            mdict['nucleus_lvl_normed_minmax'].append(norm_ncl_minmax)
            mdict['cytoplasm_lvl_normed_minmax'].append(norm_cyt_minmax)

            # compute nucleus over cytoplasm minmax ratio
            ratio_minmax = metrics.compute_normed_value(norm_ncl_minmax,
                                                        norm_cyt_minmax)
            mdict['ratio_lvl_minmax'].append(ratio_minmax)

    df = pd.DataFrame(mdict, index=index)
    if find_blue_cells:
        df = _find_blue_cells(df)
    return df.dropna(axis=0, how='any')


def _find_blue_cells(df):
    """return passed dataframe with a new boolean column blue cell,
    stating is the cell is blue or not

    Parameters:
    -----------
    df : pandas dataframe
        dataframe with information on chroma components a & b for cell regions
    """
    features = df[['cytoplasm_a', 'cytoplasm_b']].values
    kmeans = cluster.KMeans(n_clusters=2, n_init=32, max_iter=500)
    kmeans.fit(features)
    centers = kmeans.cluster_centers_
    labels = kmeans.labels_.astype(bool)
    if np.sum(centers[0] - centers[1]) < 0:
        labels = ~labels
    df['blue_cell'] = labels

    return df


if __name__ == '__main__':
    pass

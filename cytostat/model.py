# -*- coding: utf-8 -*-

import torch
import torch.nn as nn


class Unet(nn.Module):

    def __init__(self, n_class=3, n_channel=3, drop_rate=0.):
        super(Unet, self).__init__()
        self.convblock1 = ConvBlock(n_channel, 32)
        self.convblock2 = ConvBlock(32, 64)
        self.convblock3 = ConvBlock(64, 128)
        self.convblock4 = ConvBlock(128, 256)
        self.upblock1 = UpBlock(256, 128)
        self.upblock2 = UpBlock(128, 64)
        self.upblock3 = UpBlock(64, 32)
        self.out = nn.Conv2d(32, n_class, 1)

        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=1) # sotmax over channels
        self.maxpool = nn.MaxPool2d((2,2))
        self.dropout = nn.Dropout2d(p=drop_rate)

    def forward(self, x):
        x1 = self.convblock1(x)
        x2 = self.dropout(self.convblock2(self.maxpool(x1)))
        x3 = self.dropout(self.convblock3(self.maxpool(x2)))
        x = self.dropout(self.convblock4(self.maxpool(x3)))
        x = self.dropout(self.upblock1(x, x3))
        x = self.dropout(self.upblock2(x, x2))
        x = self.upblock3(x, x1)
        return self.softmax(self.out(x))


class ConvBlock(nn.Module):

    def __init__(self, input_shape, output_shape):
        super(ConvBlock, self).__init__()
        self.conv1 = nn.Conv2d(input_shape, output_shape, 3, padding=1,
                                padding_mode='reflect')
        self.conv2 = nn.Conv2d(output_shape, output_shape, 3, padding=1,
                               padding_mode='reflect')
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.relu(self.conv1(x))
        x = self.relu(self.conv2(x))
        return x


class UpBlock(nn.Module):

    def __init__(self, input_shape, output_shape):
        super(UpBlock, self).__init__()
        self.conv1 = nn.Conv2d(input_shape, output_shape, 3, padding=1,
                               padding_mode='reflect')
        self.conv2 = nn.Conv2d(2*output_shape, output_shape, 3, padding=1,
                               padding_mode='reflect')
        self.conv3 = nn.Conv2d(output_shape, output_shape, 3, padding=1,
                               padding_mode='reflect')
        self.up = nn.Upsample(scale_factor=(2,2), mode='bilinear',
                              align_corners=True)
        self.relu = nn.ReLU()

    def forward(self, x1, x2):
        x1 = self.up(x1)
        x1 = self.relu(self.conv1(x1))
        x = torch.cat([x1, x2], dim=1)
        x = self.relu(self.conv2(x))
        x = self.relu(self.conv3(x))
        return x


if __name__ == '__main__':
    pass

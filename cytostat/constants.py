# -*- coding: utf-8 -*-

BGD_MASK = 'mask_background'
CYT_MASK = 'mask_cytoplasm'
NCL_MASK = 'mask_nuclei'

BOUND_L = (0.0, 100.0)
BOUND_A = (-86.184, 98.234)
BOUND_B = (-107.858, 94.479)


if __name__ == '__main__':
    pass

# -*- coding: utf-8 -*-

import os
import numpy as np
from skimage import img_as_ubyte
import imageio
import torch
import torchvision.transforms as transforms
import pkg_resources

from . import utils, model
from .constants import BGD_MASK, NCL_MASK, CYT_MASK

DATA_PATH = pkg_resources.resource_filename('cytostat', 'checkpoints')
LOCAL_MODEL_WEIGHTS = os.path.join(DATA_PATH, 'protocol_weights.pt')


def dump_masks_local(img_folder, dump_path):
    """predict cell regions masks on the images in img_folder.

    Parameters:
    -----------
    img_folder : str
        path of the folder containing the cells images.
    dump_path : str
        path of the folder where the masks are saved
    """
    # dataset with no ground_truth : only for inference
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    dataset = utils.ImageDataset(img_folder, transform=transforms.ToTensor())
    loader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False)
    save_ncl, save_cyt, save_bgd = _create_mask_folders(dump_path)
    # predict the masks
    with torch.no_grad():
        unet = model.Unet(n_class=3, n_channel=3).to(device)
        unet.load_state_dict(torch.load(LOCAL_MODEL_WEIGHTS))
        for img, name in loader:
            name = name[0]
            pred = unet(img.to(device)).cpu().squeeze().numpy()
            mask_bgd = img_as_ubyte(np.round(pred[0, ...]))
            mask_cyt = img_as_ubyte(np.round(pred[1, ...]))
            mask_ncl = img_as_ubyte(np.round(pred[2, ...]))
            imageio.imwrite(os.path.join(save_bgd, name), mask_bgd)
            imageio.imwrite(os.path.join(save_cyt, name), mask_cyt)
            imageio.imwrite(os.path.join(save_ncl, name), mask_ncl)


def _create_mask_folders(dump_path):
    """create folder for every segmented cell region"""
    save_ncl = os.path.join(dump_path, NCL_MASK)
    if not os.path.exists(save_ncl):
        os.makedirs(save_ncl)

    save_cyt = os.path.join(dump_path, CYT_MASK)
    if not os.path.exists(save_cyt):
        os.makedirs(save_cyt)

    save_bgd = os.path.join(dump_path, BGD_MASK)
    if not os.path.exists(save_bgd):
        os.makedirs(save_bgd)

    return save_ncl, save_cyt, save_bgd


if __name__ == '__main__':
    pass

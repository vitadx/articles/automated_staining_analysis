# -*- coding: utf-8 -*-

import os
import pywt
import imageio
import torch
import numpy as np

from skimage import img_as_float32, img_as_float, img_as_ubyte, color, filters
from scipy import ndimage
from tqdm import tqdm
from sklearn import decomposition


# --- pytorch image dataset

class ImageDataset(torch.utils.data.Dataset):
    def __init__(self, folderpath, transform=None):
        super(ImageDataset, self).__init__()
        self.x = os.listdir(folderpath)
        self.folderpath = folderpath
        self.transforms = transform

    def __getitem__(self, idx):
        img = imageio.imread(os.path.join(self.folderpath, self.x[idx]))
        img = self.crop_nearest_multiple(
            img_as_float32(img), multiple=8, axis=(0,1))
        if self.transforms is not None:
            img = self.transforms(img)
        return img, self.x[idx]

    def __len__(self):
        return len(self.x)

    @staticmethod
    def crop_nearest_multiple(array, multiple=8, axis=(0,1)):
        """general cropping function"""
        if not isinstance(axis, tuple):
            axis = (axis,)
        list_slice = [slice(None,None,None) for i in range(array.ndim)]
        for a in axis:
            size = array.shape[a]
            new_size = int(multiple * (size // multiple))
            start_index = int(np.ceil((size-new_size)*0.5))
            list_slice[a] = slice(start_index, start_index+new_size, 1)
        return array[tuple(list_slice)]


# --- code for extended depth of field

def stationnary_edf(volume, wt='bior3.3', nb_level=1, s=1):
    """stationnary wavelet transform based extended depth of field algorithm

    The algorithm works for grayscale data. When using color data, the volume is
    converted to b&w using principal component analysis, then the result is
    converted back into the original space.

    Parameters:
    -----------
    volume : ndarray
        volume to be processed
    wt : str, optional
        name of the wavelet to use for the transformation. (bior3.3 by default).
    nb_level : int, optional
        level of the decomposition. (Default is 1).
    s : int, optional
        size of the maximum filter applied on the detail map. Improves
        robustness, reduces reconstruction sharpness. (Default is 1).

    Returns:
    --------
    r : 2D array
        extended depth of field image
    level_map_approx : ndarray
        array shaped as volume where 1 correspond to the location of pixels
        selected in the approximation decomposition volume
    level_map_detail : ndarray
        array shaped as volume where 1 correspond to the location of pixels
        selected in the detail decomposition volume
    """
    v = img_as_float(volume)

    min_volume = np.amin(v)
    max_volume = np.amax(v)

    if volume.ndim == 4:
        cvrt = Pca_convert(v)
        v = cvrt.transform(v)

    min_v = np.amin(v)
    v += abs(min_v) # set min of the pca to 0 to avoid negative values
    max_v = np.amax(v)

    # padding array, since dimensions should be a multiple of 2**nb_level
    check_x_axis = v.shape[1] % 2**nb_level
    if check_x_axis != 0:
        pad_x = 2**nb_level - check_x_axis
    else:
        pad_x = 0
    check_y_axis = v.shape[2] % 2**nb_level
    if check_y_axis != 0:
        pad_y = 2**nb_level - check_y_axis
    else:
        pad_y = 0
    v = np.pad(v, ((0, 0), (0, pad_x), (0, pad_y)), mode='edge')

    coefs_volumes = np.zeros((nb_level, 4, *v.shape))
    for section_index, section in enumerate(v):
        coefs = pywt.swt2(section, wt, level=nb_level, start_level=0)
        for level_index, c in enumerate(coefs):
            coefs_volumes[level_index][0][section_index] = c[0]
            coefs_volumes[level_index][1][section_index] = c[1][0]
            coefs_volumes[level_index][2][section_index] = c[1][1]
            coefs_volumes[level_index][3][section_index] = c[1][2]

    final_coefs = []
    for cv in coefs_volumes:

        approx_volume = cv[0]
        approx_var = var_laplacian(approx_volume, size=(1, 3, 3))
        var_proj = np.amax(approx_var, axis=0)
        lvl_map_approx = approx_var == var_proj

        h_volume = ndimage.filters.maximum_filter(cv[1], size=(s, s, s))
        v_volume = ndimage.filters.maximum_filter(cv[2], size=(s, s, s))
        d_volume = ndimage.filters.maximum_filter(cv[3], size=(s, s, s))

        max_detail_volume = abs(h_volume) + abs(v_volume) + abs(d_volume)
        max_detail_proj = np.amax(max_detail_volume, axis=0)
        lvl_map_detail = max_detail_proj == max_detail_volume

        lvl_map, lm_mask = combine_lvlmap(lvl_map_approx, lvl_map_detail)

        final_approx = np.amax(approx_volume*lm_mask, axis=0)

        final_h = np.amax(h_volume*lm_mask, axis=0)
        final_v = np.amax(v_volume*lm_mask, axis=0)
        final_d = np.amax(d_volume*lm_mask, axis=0)

        final_coefs.append([final_approx, (final_h, final_v, final_d)])

    r = pywt.iswt2(final_coefs, wt)
    # unpad if necessary:
    if not pad_x == 0:
        r = r[:-pad_x, :]
        lvl_map = lvl_map[:-pad_x, :]
        lm_mask = lm_mask[:, :-pad_x, :]
    if not pad_y == 0:
        r = r[:, :-pad_y]
        lvl_map = lvl_map[:, :-pad_y]
        lm_mask = lm_mask[:, :, :-pad_y]

    # color fidelity:
    r = (((r - np.min(r))/np.ptp(r)) * (max_v - 0))
    # substract min_v added before the wavelet decomposition
    r -= abs(min_v)

    if volume.ndim == 4:
        r = cvrt.inverse_transform(r)
    r = (((r - np.min(r))/np.ptp(r)) * (max_volume - min_volume)) + min_volume
    if volume.ndim == 4:
        r = color_reassign(volume, r, lm_mask)
    r = img_as_ubyte(r)

    return r, lvl_map


def combine_lvlmap(lm_approx, lm_detail):
    """Combine two level maps to create a more consistent one

    Parameters:
    -----------
    lm_approx : ndarray
        3D binary level map, where True are in-focus voxels.
    lm_detail : ndarray
        3D binary level map, where True are in-focus voxels.

    Returns:
    --------
    l : ndarray
        2D level map, where each value correspond to the plane where the
        maximum of information is located.
    lm_mask : ndarray
        3D binary level map corresponding to l.
    """
    l1 = lm_approx.copy().astype(np.uint8)
    l2 = lm_detail.copy().astype(np.uint8)
    lm_mask = np.ones_like(l1)
    for i in range(l1.shape[0]):
        l1[i] *= np.uint8(i + 1)
        l2[i] *= np.uint8(i + 1)
        lm_mask[i] *= np.uint8(i + 1)
    l1 = np.amax(l1, axis=0)
    l2 = np.amax(l2, axis=0)
    l1 = filters.median(l1, selem=np.ones((3,3)))
    l2 = filters.median(l2, selem=np.ones((3,3)))
    l = np.round((l1.astype(np.float64) + l2.astype(np.float64)) / 2.)
    l = l.astype(np.uint8)
    lm_mask = lm_mask == l

    return l-1, lm_mask


class Pca_convert(object):

    def __init__(self, volume):
        if not volume.ndim == 4:
            raise ValueError('volume is expected to be of dimension 4')
        nb_section = volume.shape[0]
        self.shape = volume.shape
        l_channels = [volume[int(volume.shape[0]/2),:,:,0],
                      volume[int(volume.shape[0]/2),:,:,1],
                      volume[int(volume.shape[0]/2),:,:,2]]
        self.data = np.stack([v.flatten() for v in l_channels], axis=1)
        self.pca = decomposition.PCA(n_components=1, copy=True)
        self.pca.fit(self.data)

    def transform(self, data):
        l_channels = [data[:,:,:,0],
                      data[:,:,:,1],
                      data[:,:,:,2]]
        flat_data = np.stack([v.flatten() for v in l_channels], axis=1)
        output_pca = self.pca.transform(flat_data)
        return output_pca.reshape(*self.shape[:-1])

    def inverse_transform(self, data):
        flat_data = np.expand_dims(data.flatten(), axis=1)
        output_pca = self.pca.inverse_transform(flat_data)
        return output_pca.reshape((*data.shape, 3))


def color_reassign(volume, edf_img, lvl_mask):
    """reassign color of the edf image

    The colors of the edf image are reassigned based on the color of the
    original voxel color, selected using the level map mask.

    Parameters:
    -----------
    volume : ndarray
        reference volume
    edf_img : ndarray:
        edf image to be processed

    Returns:
    --------
    new_edf : ndarray
        color reassigned edf image shaped as edf_img
    """
    v = img_as_float(volume)
    edf_img = img_as_float(edf_img)
    lvl_mask_3d = np.stack([lvl_mask]*3, axis=-1)
    composite = np.amax(v*lvl_mask_3d, axis=0)

    y = color.rgb2yuv(edf_img)[:, :, 0]
    u = color.rgb2yuv(composite)[:, :, 1]
    v = color.rgb2yuv(composite)[:, :, 2]

    yuv = np.stack([y, u, v], axis=-1)
    new_edf = color.yuv2rgb(yuv)
    # assert reconstruction range
    new_edf[new_edf > 1.0] = 1.0
    new_edf[new_edf < 0.0] = 0.0

    return new_edf


def var_laplacian(volume, size=3):
    """compute sharpness map based on laplacian variance on sliding window

    Highly noisy, does not seems very good for cell part differenciation

    Parameters:
    -----------
    volume : ndarray
        volume to be processed
    size : int, optional
        size of the sliding window were the variance of laplacian is computed
        (default = 3)

    Returns:
    --------
    sharpMap : ndarray
        sharpness map shape as volume
    """
    v = volume.copy()
    if v.dtype == np.uint8:
        v = v / 255
    laplace = ndimage.laplace(v)
    windowMean, window2Mean = (ndimage.uniform_filter(l, size=size) for l in
                               (laplace, laplace*laplace))
    sharpMap = abs(window2Mean - windowMean*windowMean)
    sharpMap = sharpMap / np.amax(sharpMap)

    return sharpMap


def compute_edf(path):
    """compute edf over a sequence

    Parameters:
    -----------
    path : str
        path of the sequence
    foucs_region : boolean, optional
        if True, the extended depth of filed is only computed on the estimated
        focus region, Default is False.

    Returns:
    --------
    edf_img : ndarray
        extended depth of field image
    """
    sequence = load_volume(path)
    edf_img, lvl_map = stationnary_edf(sequence)
    return edf_img, lvl_map


def compute_on_database(database_path, dump_path, edf_folder_name=None,
                        lvl_folder_name=None, with_edf=True, with_lvlmap=True):
    """compute edf on every stack in the database and save them

    Parameters:
    -----------
    database_path : str
        path of the folder containing the stack
    dump_path : str
        path of the folder where the results are written
    edf_folder_name : str, optional
        name of the edf folder. If None given, EDF is set by default.
    lvl_folder_name : str, optional
        name of the levelmpa folder, If None given, LVL is set by default
    with_edf : boolean, optional
        if True, the extended depth of field result is dumped.
    with_lvlmap : boolean, optional
        if True, the level map is dumped
    """
    assert isinstance(database_path, str)
    assert isinstance(dump_path, str)

    if edf_folder_name is None:
        edf_folder_name = 'EDF'
    if lvl_folder_name is None:
        lvl_folder_name = 'LVL'

    edf_dump = os.path.join(dump_path, edf_folder_name)
    lvl_dump = os.path.join(dump_path, lvl_folder_name)

    if with_edf or with_lvlmap:
        print('[INFO] Processing:', database_path)
        if with_edf and not os.path.exists(edf_dump):
            os.makedirs(edf_dump)
        if with_lvlmap and not os.path.exists(lvl_dump):
            os.makedirs(lvl_dump)

        for f in tqdm(os.listdir(database_path)):
            edf_img, lvlmap = compute_edf(os.path.join(database_path, f))

            if with_edf:
                out_path = os.path.join(edf_dump, f + '.png')
                imageio.imwrite(out_path, edf_img)

            if with_lvlmap:
                lvlmap = img_as_ubyte(lvlmap / np.amax(lvlmap))
                out_path = os.path.join(lvl_dump, f + '.png')
                imageio.imwrite(out_path, lvlmap)
    else:
        print('[INFO] Skipping:', database_path)


# --- load sequences as volumeio

def load_volume(path):
    """load a volume from images present in a folder or a tiff file.

    Returns a volume of images present in a given folder or tiff file. Images
    are sorted in alphanumeric order.

    Parameters:
    -----------
    path : string
        path of the folder

    Returns:
    --------
    volume : ndarray
        volume of images
    """
    if not isinstance(path, str):
        raise TypeError('string expected, not', type(path))
    if os.path.isdir(path):
        volume = _load_from_folder(path)
    elif path.lower().endswith(('tif', 'tiff')):
        volume = imageio.volread(path)
    elif not os.path.exists(path):
        raise FileNotFoundError('file not found', path)
    else:
        raise ValueError('folder expected or tiff file, got:', path)

    return volume


def _load_from_folder(folder_path):
    """load a volume from images present in the given folder

    Images are sorted in alphanumeric order.

    Parameters:
    -----------
    folder_path : string
        path of the folder containing the images

    Returns:
    --------
    volume : ndarray
        volume loaded from the images present in the folder
    """
    LIST_EXT = ['.jpg', '.jpeg', '.png', '.tif', '.tiff']
    list_arrays = []
    for file in sorted(os.listdir(folder_path)):
        if not os.path.splitext(file)[1].lower() in LIST_EXT:
            continue
        list_arrays.append(imageio.imread(os.path.join(folder_path, file)))

    return np.stack(list_arrays, axis=0)


if __name__ == '__main__':
    pass
